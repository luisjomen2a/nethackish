using UnityEngine;

public class Room : MonoBehaviour
{ 
    private RoomSize m_roomSize;

    // GameObjects floor and walls
    private GameObject m_floor;
    //private GameObject m_walls;

    // minimal and maximal size possible for the room
    private int m_minSize = 2;
    private int m_maxSize = 10;

    // Grid to be drawn on top of the room's floor
    private Grid m_grid;

    // Name given to the room to be passed to each GameObject
    private string m_name;
    
    private void Awake()
    {
        m_roomSize = new RoomSize(m_minSize, m_maxSize);

        // Create the floor GameObject with the generated room sizes
        m_floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
        m_floor.name = "roomFloor" + m_name;
        m_floor.transform.localScale = new Vector3(m_roomSize.Width, 1, m_roomSize.Length);

        // Creat tthe grid to print over the room's floor
        m_grid = m_floor.AddComponent<Grid>();
        Dimension gridDimension = new Dimension(m_roomSize.Width, m_roomSize.Length);
        m_grid.Dimensions = gridDimension;
        m_grid.SetTransform(m_floor.transform);
    }

    //-------------------------------------------------------------------------

    private void OnDestroy()
    {
        Destroy(m_floor);
        Destroy(m_grid);
    }

    // getters

    public int GetMinSize()
    {
        return m_minSize;
    }

    //-------------------------------------------------------------------------

    public int GetMaxSize()
    {
        return m_maxSize;
    }

    //-------------------------------------------------------------------------
    
    public RoomSize GetSize()
    {
        return m_roomSize;
    }

    // setters

    //-------------------------------------------------------------------------

    public void SetMinSize(int minSize)
    {
        m_minSize = minSize;
    }

    //-------------------------------------------------------------------------

    public void SetMaxSize(int maxSize)
    {
        m_maxSize = maxSize;
    }

    //-------------------------------------------------------------------------

    public void SetName(string name)
    {
        m_name = name;

        if (m_floor)
        {
            m_floor.name = "roomFloor" + name;
        }
        //TODO : gotta build those walls
        //if (m_walls)
        //{
        //    m_walls.name = "roomWalls" + name;
        //}
    }

    //-------------------------------------------------------------------------

    public Transform GetFloorTransform()
    {
        return m_floor.transform;
    }

    //-------------------------------------------------------------------------

    /*public Transform GetWallTransform()
    {
        return m_w.transform;
    }*/

    //-------------------------------------------------------------------------

    public void SetPosition(Position position)
    {
        m_floor.transform.localPosition = position.ToVector3();
        //m_walls.transform.localPosition = position.ToVector3();
    }

    //-------------------------------------------------------------------------
        
    public void SetPosition(Vector3 position)
    {
        m_floor.transform.localPosition = position;
        //m_walls.transform.localPosition = position.ToVector3();
    }

}

/// <summary>
/// A randomly generated dimension.
/// </summary>
public class RoomSize : Dimension
{
    // constructors

    public RoomSize(int minSize, int maxSize) : base(Random.Range(minSize, maxSize), Random.Range(minSize, maxSize))
    {
    }

    public RoomSize(int minSizeWidht, int maxSizeWidth, int minSizeLength, int maxSizeLength) :
        base(Random.Range(minSizeWidht, maxSizeWidth), Random.Range(minSizeLength, maxSizeLength))
    {
    }
    // getters 
    
    //-------------------------------------------------------------------------

    public int GetWidth()
    {
        return Width;
    }

    //-------------------------------------------------------------------------

    public int GetLength()
    {
        return Length;
    }

    //setters

    //-------------------------------------------------------------------------

    public void GenerateWidth(int minSize, int maxSize)
    {
        Width = Random.Range(minSize, maxSize);
    }

    //-------------------------------------------------------------------------

    public void GenerateLength(int minSize, int maxSize)
    {
        Length = Random.Range(minSize, maxSize);
    }

    //-------------------------------------------------------------------------
}


// Normal size for width and length
[System.Serializable]
public class Dimension
{
    public int m_width  = -1;
    public int m_length = -1;

    public Dimension()
    {
        m_width  = 0;
        m_length = 0;
    }

    public Dimension(Dimension toCopy)
    {
        m_width = toCopy.Width;
        m_length = toCopy.Length;
    }

    public Dimension(int width, int length)
    {
        if(width < 0)
        {
            m_width = 0;
        }
        if (length < 0)
        {
            m_length = 0;
        }

        m_width = width;
        m_length = length;
    }

    public int Width
    {
        get
        {
            return m_width;
        }

        set
        {
            if (value > 0)
            {
                m_width = value;
            }
            else
            {
                m_width = 0;
            }
        }
    }

    public int Length
    {
        get
        {
            return m_length;
        }

        set
        {
            if(value > 0)
            {
                m_length = value;
            }
            else
            {
                m_length = 0;
            }
        }
    }

    public override string ToString()
    {
        string result = new string(new char[] { });

        result += "Width : ";
        result += m_width;
        result += ", Length : ";
        result += m_length;
        result += "\n";

        return result;
    }
}

/// <summary>
/// Position on a Level, it can only be positive coordinates
/// </summary>
[System.Serializable]
public class Position
{
    public int m_posX = -1;
    public int m_posZ = -1;

    public int PosX
    {
        get
        {
            return m_posX;
        }

        set
        {
            m_posX = value;
        }
    }

    public int PosZ
    {
        get
        {
            return m_posZ;
        }

        set
        {
            m_posZ = value;
        }
    }

    public Position(int posX, int posY)
    {
        if (posX < 0)
        {
            m_posX = 0;
        }
        if (posY < 0)
        {
            PosZ = 0;
        }

        m_posX = posX;
        PosZ = posY;
    }

    public Vector3 ToVector3()
    {
        return new Vector3(m_posX, Constants.AbsoluteY+1, m_posZ);
    }

    public override string ToString()
    {
        string result = new string(new char[] { });

        result += "PosX : ";
        result += m_posX;
        result += ", PosZ : ";
        result += m_posZ;
        result += "\n";

        return result;
    }

}
