using UnityEngine;

public class Grid : MonoBehaviour
{
    // When added to an object, draws a grid on top of it. (sizes are not calculated for now)
    private Dimension m_dimensions;

    private Position m_positon = new Position(0,0);
    
    private Transform m_gridTransform;

    private Color m_color = new Color(0,0,0);

    // Getters
    //-------------------------------------------------------------------------

    public int GetWidth()
    {
        return m_dimensions.Width;
    }

    //-------------------------------------------------------------------------

    public int GetHeight()
    {
        return m_dimensions.Length;
    }

    //-------------------------------------------------------------------------

    public Transform GetTransform()
    {
        return m_gridTransform;
    }

    // Setters
    //-------------------------------------------------------------------------

    public void SetLenght(int length)
    {
        m_dimensions.Length = length;
    }

    //-------------------------------------------------------------------------

    public void SetWidth(int width)
    {
        m_dimensions.Width = width; 
    }

    //-------------------------------------------------------------------------

    public void SetTransform(Transform gridTransform)
    {
        m_gridTransform = gridTransform;
    }

    //-------------------------------------------------------------------------

    public void SetColor(Color color)
    {
        m_color = color;
    }

    //-------------------------------------------------------------------------

    static Material lineMaterial;

    public Dimension Dimensions
    {
        get
        {
            return m_dimensions;
        }

        set
        {
            m_dimensions = value;
        }
    }

    public Position Positon
    {
        get
        {
            return m_positon;
        }

        set
        {
            m_positon = value;
        }
    }

    static void CreateLineMaterial()
    {
        if (!lineMaterial)
        {
            // Unity has a built-in shader that is useful for drawing
            // simple colored things.
            Shader shader = Shader.Find("Hidden/Internal-Colored");
            lineMaterial = new Material(shader);
            lineMaterial.hideFlags = HideFlags.HideAndDontSave;
            // Turn on alpha blending
            lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
            lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
            // Turn backface culling off
            lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
            // Turn off depth writes
            lineMaterial.SetInt("_ZWrite", 0);
        }
    }

    // Will be called after all regular rendering is done
    public void OnRenderObject()
    {
        CreateLineMaterial();
        // Apply the line material
        lineMaterial.SetPass(0);

        //GL.PushMatrix();
        // Set transformation matrix for drawing to
        // match our transform
        //GL.MultMatrix(transform.localToWorldMatrix);

        // Draw Lines;
        GL.Begin(GL.QUADS);
        GL.Color(m_color);
        

        // Affect the position of the object.
        // The grid is drawn from a corner whereas the object it's drawn from it's center. 
        float xOffset = m_gridTransform.position.x - (float)(m_dimensions.Width)/2f;
        float yOffset = m_gridTransform.position.y - 0.48f;
        float zOffset = m_gridTransform.position.z - (float)(m_dimensions.Length)/2f;

        float thickness = 0.02f;

        //Debug.Log("x " + xOffset + " y " + yOffset + " z " + zOffset);

        for (float i = m_positon.PosX; i <= m_dimensions.Length + m_positon.PosX; i++)
        {
            GL.Vertex3(xOffset , yOffset + 1, zOffset + i - thickness);
            GL.Vertex3(xOffset , yOffset + 1, zOffset + i + thickness);
            GL.Vertex3(xOffset + m_dimensions.Width, yOffset + 1, zOffset + i+ thickness);
            GL.Vertex3(xOffset + m_dimensions.Width, yOffset + 1, zOffset + i- thickness);
        }

        //Z axis lines
        for (float i = m_positon.PosZ; i <= m_dimensions.Width + m_positon.PosZ; i++)
        {
            GL.Vertex3(xOffset + i - thickness, yOffset + 1, zOffset );
            GL.Vertex3(xOffset + i + thickness, yOffset + 1, zOffset );
            GL.Vertex3(xOffset + i + thickness,yOffset + 1,zOffset + m_dimensions.Length );
            GL.Vertex3(xOffset + i - thickness,yOffset + 1,zOffset + m_dimensions.Length );
        }

        GL.End();
    }
}
