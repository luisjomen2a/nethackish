using UnityEngine;
using System.Collections.Generic;

public class Unit
{
    public enum UnitType
    {
        Room,
        Tunnel,
        Wall,
        Rock
    };

    private int m_positionX;
    private int m_positionZ;
        
    private UnitType m_unitType;

    public Unit(int positionX, int positionZ, UnitType unitType)
    {
        m_positionX = positionX;
        m_positionZ = positionZ;

        m_unitType = unitType;
    }

    public int PositionZ
    {
        get
        {
            return m_positionZ;
        }

        set
        {
            m_positionZ = value;
        }
    }

    public int PositionX
    {
        get
        {
            return m_positionX;
        }

        set
        {
            m_positionX = value;
        }
    }

    public UnitType GetUnitType()
    {
        return m_unitType;
    }

    public void SetUnitType(UnitType unitType)
    {
        m_unitType = unitType;
    }
}

public class UnitMap
{
    private List<Unit> m_unitMap;

    Dimension m_dimension;
    
    //-------------------------------------------------------------------------
    // Basic cunstructor
    public UnitMap(Dimension dimension)
    {
        m_dimension = new Dimension(dimension.Width, dimension.Length);

        m_unitMap = new List<Unit>(m_dimension.Width * m_dimension.Length);
    }

    //-------------------------------------------------------------------------
    // Copy Constructor
    public UnitMap(UnitMap toCopy)
    {
        m_dimension = new Dimension(toCopy.Dimension.Width, toCopy.Dimension.Length);

        m_unitMap = new List<Unit>(m_dimension.Width * m_dimension.Length);

        for (int i = 0; i< m_dimension.Width; i++)
        {
            for (int j = 0; j < m_dimension.Length; j++)
            {
                SetUnitMapAt(i, j, toCopy.GetUnitMapAt(i, j));
            }
        }
    }

    //-------------------------------------------------------------------------
    
    public Dimension Dimension
    {
        get
        {
            return m_dimension;
        }

        set
        {
            m_dimension = value;
        }
    }

    //-------------------------------------------------------------------------

    public Unit GetUnitMapAt(int x, int z)
    {
        return m_unitMap[x * m_dimension.Length + z];
    }

    //-------------------------------------------------------------------------

    public void SetUnitMapAt(int x, int z, Unit unit)
    {
        m_unitMap.Insert(x * m_dimension.Length + z, unit);
    }

    //-------------------------------------------------------------------------
    /// <summary>
    /// Fill the whole UnitMap with the given type.
    /// </summary>
    /// <param name="type"> Type to be affected to the whole UnitMap </param>
    public void Generate(Unit.UnitType type)
    {
        Fill(m_dimension, new Position(0,0), type, true);
    }

    //-------------------------------------------------------------------------

    public bool Fill(Dimension dimension, Position position, Unit.UnitType type, bool generate = false)
    {
        //  position are out of bounds.
        if(position.PosX > m_dimension.Width || position.PosZ > m_dimension.Length)
        {
            return false;
        }

        if(dimension.Width + position.PosX > m_dimension.Width)
        {
            dimension.Width = m_dimension.Width - position.PosX;
        }
        if (dimension.Length + position.PosZ > m_dimension.Length)
        {
            dimension.Length = m_dimension.Length - position.PosZ;
        }

        for (int i = position.PosX; i < dimension.Width + position.PosX; i++)
        {
            for (int j = position.PosZ; j < dimension.Length + position.PosZ; j++)
            {
                if(!generate)
                {
                    m_unitMap[i*m_dimension.Length + j].SetUnitType(type);
                }
                else
                {
                    Unit unitToAdd = new Unit(i, j, type);

                    m_unitMap.Add(unitToAdd);
                }
            }
        }

        return true;
    }

    //-------------------------------------------------------------------------

    public override string ToString()
    {
        string result = new string(new char[] { '\n' });

        // So that the printing looks 'correct' we must switch Length and Width.
        for (int z = 0; z < m_dimension.m_length; z++)
        {
            for (int x = 0; x < m_dimension.m_width; x++)
            {
                result += GetUnitMapAt(x, z).GetUnitType();
                if(x < m_dimension.m_width-1)
                {
                    result += " - ";
                }
            }
            result += "\n";
        }

        return result;
    }

    //-------------------------------------------------------------------------

    /// <summary>
    ///  Find the maximum free area (Units markes as 'Rock') where a new room could be fited.
    ///  The returned square should be at minimum as big as minDimension
    ///  (Note : it's not a matter of size, length*width, but a matter of shape!)
    /// </summary>
    /// <returns>A unit map corresponding to maximum free area</returns>
    public MapArea FindFreeSquare(Dimension minDimension)
    {
        for (int i = 0; i < m_dimension.Width; i++)
        {
            for (int j = 0; j < m_dimension.Length; j++)
            {
                Unit currentUnit = GetUnitMapAt(i, j);

                if (currentUnit.GetUnitType() == Unit.UnitType.Rock)
                {
                    Position currentPositon = new Position(i, j);

                    Dimension currentDimension = FindFreeSquareSize(currentPositon);

                    // Check if we've met the requirements for our dimensions.
                    if(currentDimension.Width > minDimension.Width 
                    && currentDimension.Length > minDimension.Length)
                    {
                        return new MapArea(currentDimension, currentPositon);
                    }
                }
            }
        }
        // if no square big enough was found, then an empty square should be returned
        return new MapArea();

    }

    //-------------------------------------------------------------------------

    /// <summary>
    /// Find the size of a square starting from the given position x and y.
    /// </summary>
    /// <returns> return the size in Units of the square starting from the given position x and y </returns>
    public Dimension FindFreeSquareSize(Position position)
    {
        Dimension result = new Dimension();

        int maximalLength = int.MaxValue;
        int maximalWidth  = 0;

        for (int i = position.PosX; i < m_dimension.Width; i++)
        {
            int tempLength = 0;

            for (int j = position.PosZ; j < m_dimension.Length ; j++)
            {                
                // if this rock is the correct type to be filled then count it as a possible length
                if (GetUnitMapAt(i, j).GetUnitType() == Unit.UnitType.Rock)
                {
                    tempLength++;
                }
                else // if this is already a room or something else ...
                {
                    //and if this is the first iteration of j, ie we have reached the first row again...
                    if (j == position.PosZ)
                    {
                        // then maximalWidth is reached and maximal length should already at it's maximal possibility
                        result.Length = maximalLength;
                        result.Width = maximalWidth;
                        return result;
                    }
                    // if tempLength is smaller than the already calculated max value, then it mean temp adds more 
                    // constraints to the final square and thus it becomes the max value possible.
                    if (tempLength < maximalLength)
                    {
                        maximalLength = tempLength;
                    }
                }
            }
            maximalWidth++;

            if (tempLength < maximalLength)
            {
                maximalLength = tempLength;
            }
        }
        // if we reach this, then the square was as big as the rest of the room and we can simply return both values.
        result.Length = maximalLength;
        result.Width = maximalWidth;

        return result;
    }

    /// <summary>
    /// Get the fill status of this UnitMap, 'empty' units have type 'Rock'
    /// </summary>
    public float FillPercentage()
    {
        float result = 0;

        for(int i = 0; i < m_dimension.Width; i++)
        {
            for (int j = 0; j < m_dimension.Length; j++)
            {
                if (GetUnitMapAt(i,j).GetUnitType() != Unit.UnitType.Rock)
                {
                    result++;
                }
            }
        }

        return result / (m_dimension.Width * m_dimension.Length);
    }

}

/// <summary>
/// Defines an area on the level.
/// </summary>
[System.Serializable]
public class MapArea
{
    public Dimension m_dimension;
    public Position m_position;

    public MapArea()
    {
        m_dimension = new Dimension();
        m_position = new Position(0, 0);
    }

    public MapArea(Dimension dimension, Position position)
    {
        m_dimension = new Dimension(dimension.Width, dimension.Length);
        m_position = new Position(position.PosX, position.PosZ);
    }

    public MapArea(int width, int length, int posX, int posY)
    {
        m_dimension = new Dimension(width, length);
        m_position = new Position(posX, posY);
    }

    public Dimension Dimension
    {
        get
        {
            return m_dimension;
        }

        set
        {
            m_dimension = value;
        }
    }

    public Position Position
    {
        get
        {
            return m_position;
        }

        set
        {
            m_position = value;
        }
    }

    public Position GenerateRandomPosition()
    {
        return new Position(Random.Range(m_position.PosX, m_position.PosX + m_dimension.Width),
                            Random.Range(m_position.PosZ, m_position.PosZ + m_dimension.Length));
    }
    public override string ToString()
    {
        string result = new string(new char[] { });

        result += m_dimension.ToString();
        result += m_position.ToString();

        return result;
    }

}

static class Constants
{
    public const double Pi = 3.14159;
    public const int AbsoluteY = -1; //Defines the "ground" position

}
