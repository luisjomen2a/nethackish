using UnityEngine;
using System.Collections.Generic;

public class Level : MonoBehaviour
{
    // Room contained by this level
    private List<Room> m_roomList;

    // Ground gameObject of this level
    private GameObject m_ground;
    
    // Logical UnitMap, its dimensions should be the same as ground
    private UnitMap m_unitMap;

    // Modifiable material for the ground game object
    public Material m_groudMaterial;
    
    public Dimension m_dimension;

    // Has this level been generated yet.
    private bool m_isGenerated;

    // Minimum number of rooms that will be generated in this level.
    public int m_thresholdsRooms;
    // Minimum fill percentage of the level (Room-wise)
    [Range(0, 1)]
    public float m_fillPercentage;

    // Use this for initialization
    void Start ()
    {
        //Create and position the ground's GameObject
        m_ground = GameObject.CreatePrimitive(PrimitiveType.Cube);

        m_ground.transform.localScale = new Vector3(m_dimension.Width, 1, m_dimension.Length);
        m_ground.transform.position = new Vector3((float)m_dimension.Width/ 2.0f, Constants.AbsoluteY, (float)m_dimension.Length/2.0f);
        m_ground.name = "Ground";
        m_ground.GetComponent<Renderer>().material = m_groudMaterial;
        
        // grid for ground Units
        Grid grid = m_ground.AddComponent<Grid>();
        
        grid.Dimensions = m_dimension;
        grid.SetTransform(m_ground.transform);
        
        // create the logical Unit map
        m_unitMap = new UnitMap(m_dimension);

        m_unitMap.Generate(Unit.UnitType.Rock);

        // Initialise the room List and fill it gameObjects
        m_roomList = new List<Room>();

        //AddRooms();

        m_isGenerated = false;
    }
        
    // Update is called once per frame
    void Update ()
    {
        if (!m_isGenerated)
        {
            while ( m_roomList.Count < m_thresholdsRooms || m_unitMap.FillPercentage() < m_fillPercentage)
            {
                int newIndex = AddRoom();

                Room newRoom = m_roomList[newIndex];
                                
                // Get the first found free square with at least our newRoom's dimension
                MapArea usableMapArea = m_unitMap.FindFreeSquare(new Dimension(newRoom.GetSize()));

                // if we could fit our current newRoom to our level.
                if (usableMapArea.Dimension.Width != 0 && usableMapArea.Dimension.Length != 0)
                {
                    int mapAreaPosX = usableMapArea.Position.PosX;
                    int mapAreaPosZ = usableMapArea.Position.PosZ;

                    // Generate random positon on the free square. 
                    // The last part determines how much space we have left in our square.
                    Position randomPos = new Position(
                                Random.Range(mapAreaPosX,
                                             mapAreaPosX + usableMapArea.Dimension.Width - newRoom.GetSize().Width)
                              , Random.Range(mapAreaPosZ,
                                             mapAreaPosZ + usableMapArea.Dimension.Length- newRoom.GetSize().Length));

                    // Get the position to the bottom left Vertex
                    Vector3 realPosition = new Vector3(randomPos.PosX + (float)newRoom.GetSize().Width/2.0f
                                                     , Constants.AbsoluteY+1
                                                     , randomPos.PosZ + (float)newRoom.GetSize().Length/2.0f);
                   
                    newRoom.SetPosition(realPosition);

                    // Calculate the position to fill, border with is the safe limit where ohter rooms won't be placed
                    Dimension roomAndWallsDim = new Dimension();
                    Position roomAndWallsPos = new Position(0,0);

                    int borderWidth = 3;

                    // Update the unitMap, assert that we are not going further than the uintMap's dimension
                    if (randomPos.PosX + newRoom.GetSize().Width < m_dimension.Width - borderWidth)
                    {
                        roomAndWallsDim.Width = newRoom.GetSize().Width + borderWidth;
                    }
                    else
                    {
                        // if we are going further than the unitMap's dimension we just fill what is left.
                        roomAndWallsDim.Width = m_dimension.Width - mapAreaPosX + newRoom.GetSize().Width;
                    }
                    if (randomPos.PosZ + newRoom.GetSize().Length < m_dimension.Length - borderWidth)
                    {
                        roomAndWallsDim.Length = newRoom.GetSize().Length + borderWidth;
                    }
                    else
                    {
                        roomAndWallsDim.Length = m_dimension.Length - mapAreaPosZ + newRoom.GetSize().Length ;
                    }

                    // Update the unitMap, assert that we are not going further than the uintMap's position
                    if (randomPos.PosX > borderWidth)
                    {
                        roomAndWallsPos.PosX = randomPos.PosX - borderWidth;
                        roomAndWallsDim.Width += borderWidth;
                    }
                    else
                    {
                        // if we are going further than the unitMap's dimension we just fill what is left.
                        roomAndWallsPos.PosX = 0;
                        roomAndWallsDim.Width += randomPos.PosX;
                    }
                    if (randomPos.PosZ > borderWidth)
                    {
                        roomAndWallsPos.PosZ = randomPos.PosZ - borderWidth;
                        roomAndWallsDim.Length += borderWidth;
                    }
                    else
                    {
                        // if we are going further than the unitMap's dimension we just fill what is left.
                        roomAndWallsPos.PosZ = 0;
                        roomAndWallsDim.Length += randomPos.PosZ;
                    }

                    m_unitMap.Fill(roomAndWallsDim, roomAndWallsPos, Unit.UnitType.Room);
                }
                else
                {
                    m_roomList.Remove(newRoom);

                    Destroy(newRoom);
                }
            }
            m_isGenerated = true;
        }

    }

    //------------------------------------------------------------------------- 

    /// <summary>
    /// Adds rooms components to the ground GameObject and fills the roomList with them.
    /// </summary>
    private void AddRooms()
    {
        for(int i = 0; i < m_thresholdsRooms ; i++)
        { 
            // Create the room
            Room roomToAdd = m_ground.AddComponent<Room>() as Room;

            roomToAdd.SetName(i.ToString());
           
            m_roomList.Add(roomToAdd);
        }
    }

    //-------------------------------------------------------------------------

    /// <summary>
    /// Adds a new room and return it's new index.
    /// </summary>  
    /// <returns> the new index in the m_roomList of the generated room</returns>
    private int AddRoom()
    {
        // Create the room
        Room roomToAdd = m_ground.AddComponent<Room>() as Room;
        
        //get the current size (new index)
        int resultIndex = m_roomList.Count;

        roomToAdd.SetName(resultIndex.ToString());

        m_roomList.Add(roomToAdd);

        return resultIndex;
    }
}
